import request from '@/utils/request'

// 查询文件列表
export function fileList(query) {
    let { pageNum, pageSize } = query;
    return request({
        url: '/system/miniofile/list',
        method: 'get',
        params: {
            pageNum,
            pageSize,
        },
    });
}

// 文件上传  file 
export function uploadFile(data) {
  return request({
    url: '/system/miniofile/upload',
    method: 'post',
    data: data
  })
}

// 文件下载   fileOriginName
export function downloadFile(data) {
  return request({
    url: '/system/miniofile/download',
    method: 'post',
    data: data
  })
}
