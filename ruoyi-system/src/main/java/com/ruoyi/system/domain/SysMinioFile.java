package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 【请填写功能名称】对象 sys_minio_file
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public class SysMinioFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件id */
    private Long fileId;

    /** minio中的文件名称 */
    @Excel(name = "minio中的文件名称")
    private String fileName;

    /** 文件的原始名称 */
    @Excel(name = "文件的原始名称")
    private String fileOriginName;

    /** 文件下载路径 */
    @Excel(name = "文件下载路径")
    private String fileUrl;

    /** 文件一级类型(img,word,pdf) */
    @Excel(name = "文件一级类型(img,word,pdf)")
    private String fileTypeOne;

    /** 文件二级类型(png,jpg,jif)	 */
    @Excel(name = "文件二级类型(png,jpg,jif)	")
    private String fileTypeTwo;

    public void setFileId(Long fileId) 
    {
        this.fileId = fileId;
    }

    public Long getFileId() 
    {
        return fileId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setFileOriginName(String fileOriginName) 
    {
        this.fileOriginName = fileOriginName;
    }

    public String getFileOriginName() 
    {
        return fileOriginName;
    }
    public void setFileUrl(String fileUrl) 
    {
        this.fileUrl = fileUrl;
    }

    public String getFileUrl() 
    {
        return fileUrl;
    }
    public void setFileTypeOne(String fileTypeOne) 
    {
        this.fileTypeOne = fileTypeOne;
    }

    public String getFileTypeOne() 
    {
        return fileTypeOne;
    }
    public void setFileTypeTwo(String fileTypeTwo) 
    {
        this.fileTypeTwo = fileTypeTwo;
    }

    public String getFileTypeTwo() 
    {
        return fileTypeTwo;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("fileId", getFileId())
            .append("fileName", getFileName())
            .append("fileOriginName", getFileOriginName())
            .append("fileUrl", getFileUrl())
            .append("fileTypeOne", getFileTypeOne())
            .append("fileTypeTwo", getFileTypeTwo())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
