package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysMinioFile;
import org.springframework.web.multipart.MultipartFile;

/**
 * 【请填写功能名称】Service接口
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
public interface ISysMinioFileService 
{

    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysMinioFile 【请填写功能名称】
     * @return 【请填写功能名称】集合
     */
    public List<SysMinioFile> selectSysMinioFileList(SysMinioFile sysMinioFile);

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysMinioFile 【请填写功能名称】
     * @return 结果
     */
    public int uploadSysFile(SysMinioFile sysMinioFile, MultipartFile file);

}
