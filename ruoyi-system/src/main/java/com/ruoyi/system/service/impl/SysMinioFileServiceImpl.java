package com.ruoyi.system.service.impl;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.SysMinioFile;
import com.ruoyi.system.mapper.SysMinioFileMapper;
import com.ruoyi.system.service.ISysMinioFileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 【请填写功能名称】Service业务层处理
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@Service
public class SysMinioFileServiceImpl implements ISysMinioFileService 
{
    @Autowired
    private SysMinioFileMapper sysMinioFileMapper;




    /**
     * 查询【请填写功能名称】列表
     * 
     * @param sysMinioFile 【请填写功能名称】
     * @return 【请填写功能名称】
     */
    @Override
    public List<SysMinioFile> selectSysMinioFileList(SysMinioFile sysMinioFile)
    {
        return sysMinioFileMapper.selectSysMinioFileList(sysMinioFile);
    }

    /**
     * 新增【请填写功能名称】
     * 
     * @param sysMinioFile 【请填写功能名称】
     * @return 结果
     */
    @Override
    public int uploadSysFile(SysMinioFile sysMinioFile, MultipartFile file)
    {

        sysMinioFile.setCreateTime(DateUtils.getNowDate());
        return sysMinioFileMapper.insertSysMinioFile(sysMinioFile);
    }

}
