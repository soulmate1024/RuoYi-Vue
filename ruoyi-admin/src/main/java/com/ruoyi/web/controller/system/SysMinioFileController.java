package com.ruoyi.web.controller.system;

import java.io.InputStream;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.annotation.Anonymous;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.web.core.config.MinioUtil;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysMinioFile;
import com.ruoyi.system.service.ISysMinioFileService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.web.multipart.MultipartFile;

/**
 * 【请填写功能名称】Controller
 * 
 * @author ruoyi
 * @date 2023-02-18
 */
@RestController
@RequestMapping("/system/miniofile")
public class SysMinioFileController extends BaseController
{
    @Autowired
    private ISysMinioFileService sysMinioFileService;

    @Autowired
    private MinioUtil minioUtil;

    /**
     * 查询【请填写功能名称】列表
     */
//    @PreAuthorize("@ss.hasPermi('system:file:list')")
    @GetMapping("/list")
    @Anonymous
    public TableDataInfo list(SysMinioFile sysMinioFile)
    {
        startPage();
        List<SysMinioFile> list = sysMinioFileService.selectSysMinioFileList(sysMinioFile);
        return getDataTable(list);
    }



    /**
     * 文件上传
     */
//    @PreAuthorize("@ss.hasPermi('system:file:upload')")
    @Anonymous
    @Log(title = "【请填写功能名称】", businessType = BusinessType.INSERT)
    @PostMapping("/upload")
    public AjaxResult upload(MultipartFile file) {

        SysMinioFile sysMinioFile = new SysMinioFile();
        sysMinioFile.setCreateBy("admin");
        Map<String, String> uploadFile = minioUtil.uploadFile(file, "bucket-name");
        sysMinioFile.setCreateTime(DateUtils.getNowDate());
        sysMinioFile.setFileName(uploadFile.get("fileName"));
        sysMinioFile.setFileOriginName(uploadFile.get("originalFilename"));
        sysMinioFile.setFileUrl(uploadFile.get("fileUrl"));
        sysMinioFile.setFileUrl(uploadFile.get("fileUrl"));
        sysMinioFile.setFileTypeOne("img");
        sysMinioFile.setFileTypeTwo("jpg");
        return toAjax(sysMinioFileService.uploadSysFile(sysMinioFile,file));
    }

    /**
     * 文件下载
     */
//    @PreAuthorize("@ss.hasPermi('system:file:download')")
    @Anonymous
    @PostMapping("/download")
    public void download(@RequestBody SysMinioFile sysMinioFile, HttpServletResponse response) {
        try {
            List<SysMinioFile> list = sysMinioFileService.selectSysMinioFileList(sysMinioFile);
            SysMinioFile sysFile1 = list.get(0);
            InputStream inputStream = minioUtil.getObject("bucket-name", sysFile1.getFileName());
            response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
            FileUtils.setAttachmentResponseHeader(response, sysFile1.getFileOriginName());
            ServletOutputStream outputStream = response.getOutputStream();
            byte[] b = new byte[1024];
            int length;
            while ((length = inputStream.read(b)) > 0)
            {
                outputStream.write(b, 0, length);
            }
        } catch (Exception e) {

        }
    }


}
